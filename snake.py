import random


# Se avsnitt 'Variabler' i canvas-modulen för mer information
WIDTH = 400
HEIGHT = 400

BOX_SIZE = (10, 10)
BACKGROUND_COLOR = (0, 0, 0) #röd
SNAKE_COLOR = (255, 255, 255)
FOOD_COLOR = (0, 0, 255)
UPDATE_TIME = 1/50

snake = {}


# Se avsnitt 'generate_rect'
def generate_rect(x, y):
    """
    Genererar en Rect på position x, y och returnerar det
    """
    rect = Rect(x, y, *BOX_SIZE)
    return rect


# Se avsnitt 'draw'
def draw():
    """
    Kallas 60 gånger vajer sekund. Skriver ut varje rektangel med
    angiven färg.
    """
    screen.fill(BACKGROUND_COLOR)
    for box in snake['body']:
        screen.draw.filled_rect(box, SNAKE_COLOR) #blå
    screen.draw.filled_rect(generate_rect(*snake['food']), FOOD_COLOR)
    screen.draw.text("Score: %d" %snake['score'], (0, 0))

# Se avsnitt 'update'
def update(time):
    """
    Flyttar varje låda i riktningen direction
    """
    global snake, BOX_SIZE, WIDTH, HEIGHT
    if len(snake) == 0: new_game()
    if snake['time_passed'] >= UPDATE_TIME and snake['alive']:
        snake['body'].pop(0)
        x = (snake['body'][-1].x + BOX_SIZE[0] * snake['direction'][0]) % WIDTH
        y = (snake['body'][-1].y + BOX_SIZE[1] * snake['direction'][1]) % HEIGHT
        snake['body'].append(generate_rect(x, y))
        if (x,y) == snake['food']:
            snake['score'] += 1
            snake['food'] = generate_food
            snake['body'].insert(0, generate_rect(snake['body'][0].x, \
                                                  snake['body'][0].y))
        for box in snake['body']:
            if box in snake['body'][:-1]:
                if (x, y) == (box.x, box.y) and snake['started']:
                    snake['alive'] = False
        snake['last_direction'] = snake['direction']
        snake['time_passed'] = 0
    elif snake['alive']:
        snake['time_passed'] += time
    
    if type(snake['food']) != tuple:
        snake['food'] = snake['food']()

# Se avsnitt 'on_key_down'
def on_key_down(key):
    """
    Kallas varje gång en knapp trycks ner. Om det är någon av
    piltangenterna uppdateras förflyttningsriktningen efter det.
    """
    global snake
    if not snake['started']: snake['started'] = True
    what_to_do = {keys.UP: (0, -1),
                  keys.DOWN: (0, 1),
                  keys.LEFT: (-1, 0),
                  keys.RIGHT: (1, 0)}
    try:
        if what_to_do[key] != tuple([-1 * e for e in snake['last_direction']]):
            snake['direction'] = what_to_do[key]
    except KeyError:
        if key == keys.R:
            snake = {}

def generate_food():
    food = (-1, -1)
    while food == (-1, -1):
        food = (random.randint(0, (WIDTH//BOX_SIZE[0]) -1) * BOX_SIZE[0], \
                random.randint(0, (HEIGHT//BOX_SIZE[1]) -1) * BOX_SIZE[1])
        for box in snake['body'][:-1]:
            if food == (box.x, box.y):
                food = (-1, -1)
                break
    return food

def new_game():
    global snake
    snake = {
    "body": [generate_rect(WIDTH//2, HEIGHT//2) for e in range(10)],
    "direction": (0, 0),
    "last_direction": (0, 0),
    "food": generate_food,
    "score": 0,
    "alive": True,
    "started": False,
    "time_passed": 0
    }
